import './App.css';

import SideBarCool from './Components/SideBar/SideBar';
import ModeloMM1 from './TeoriaColas/MM1/modeloMM1';
import ModeloMMS from './TeoriaColas/MMS/modeloMMS';
import ModeloMG1 from './TeoriaColas/MG1/modeloMG1';
import ModeloMD1 from './TeoriaColas/MD1/modeloMD1';
import Bienvenido from './Components/Bienvenida/Bienvenida';
import TeoriaDesicion from './TeoriaDesicion/teoriaDesicion';

import { createBrowserRouter, RouterProvider } from "react-router-dom";

const router = createBrowserRouter([
  {
    path: "/",
    element: <SideBarCool />,
    children: [
      {
        path: "/",
        element: <Bienvenido />,
      },
      {
        path: "/TeoriaDesicion",
        element: <TeoriaDesicion />,
      },
      {
        path: "/TeoriaColas/MM1",
        element: <ModeloMM1 />,
      },
      {
        path: "/TeoriaColas/MMS",
        element: <ModeloMMS />,
      },
      {
        path: "/TeoriaColas/MG1",
        element: <ModeloMG1 />,
      },
      {
        path: "/TeoriaColas/MD1",
        element: <ModeloMD1 />,
      },
    ],
  },
]);


function App() {
  return (
    <div className='App'>
      <RouterProvider router={router} />
    </div>
  )
}

export default App;

