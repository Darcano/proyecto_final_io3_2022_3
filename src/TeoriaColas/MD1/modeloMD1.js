import "./MD1.css";
import { useState } from 'react';

import poissonRandom from "../../Components/Distribuciones/poisson";

function ModeloMD1() {
    const [tLlegada, setLlegada] = useState(1.0);
    const [tServicio, setServicio] = useState(2.0);

    const [factorU, setFactorU] = useState("0");
    const [pO, setPO] = useState("0");
    const [lS, setLS] = useState("0");
    const [lq, setLq] = useState("0");
    const [wS, setWS] = useState("0");
    const [wQ, setWQ] = useState("0");

    const [minSimulacion, setminSimulacion] = useState(100.0);
    const [rangoSimulacion, setRangoSimulacion] = useState(20.0);

    const [bEjecuta, setBEjecuta] = useState(false);

    function aleatorio() {
        let auxLlegada = 0.0;
        let auxServicio = 0.0;
        do {
            auxLlegada = parseFloat((Math.random() * 5).toFixed(4));
            auxServicio = parseFloat((Math.random() * 10).toFixed(4));
        } while ((auxLlegada / auxServicio) < 0 || (auxLlegada / auxServicio) > 1);
        setLlegada(auxLlegada);
        setServicio(auxServicio);

    }

    function ordenar(event) {
        if (tLlegada > 0 && tServicio > 0 && (minSimulacion % rangoSimulacion) == 0) {
            let auxfactorU = tLlegada / tServicio;
            let auxpO = 1.0 - auxfactorU;
            let auxlq = (Math.pow(auxfactorU, 2)) / (2.0 * (1.0 - auxfactorU));
            let auxlS = auxlq + auxfactorU;
            let auxwS = auxlS / tLlegada;
            let auxwQ = auxlq / tLlegada;
            setFactorU(auxfactorU);
            setPO(auxpO);
            setLS(auxlS);
            setLq(auxlq);
            setWS(auxwS);
            setWQ(auxwQ);

            setBEjecuta(true);
        }
    }

    function limpiar() {
        setLlegada(0);
        setServicio(0);
        setFactorU(0.0);
        setPO(0.0);
        setLS(0.0);
        setLq(0.0);
        setWS(0.0)
        setWQ(0.0);
        setBEjecuta(false);
    }

    function mostrarCaracterizacion() {
        return (
            <div className="Caracter-MD1">
                <h1>Caracterización de la cola</h1>
                <a>Tasa promedio de llegada: {tLlegada.toFixed(4)} clientes/minuto</a>
                <a>Tasa promedio de servicio: {tServicio.toFixed(4)} clientes/minuto</a>
                <a>Factor Utilizacion(ρ): {factorU.toFixed(4)}</a>
                <a>Probabilidad que no existan clientes en el sistema(P0): {pO.toFixed(4)}</a>
                <a>Número promedio de clientes en el sistema(Ls): {lS.toFixed(4)} clientes</a>
                <a>Número promedio de clientes en cola(Lq): {lq.toFixed(4)} clientes</a>
                <a>Tiempo promedio clientes en el sistema(Ws): {wS.toFixed(4)} minutos</a>
                <a>Tiempo promedio clientes en cola(Wq): {wQ.toFixed(4)} minutos</a>
            </div>)
    }

    function mostrarSimulacion() {
        let registro = [];
        let auxMin = 0;
        let promedioLlegadaTotal = 0;
        let promedioServicioTotal = 0;

        for (let index = 0; index < (minSimulacion / rangoSimulacion); index++) {
            registro.push({
                clientes: [],
                promedioLlegada: 0.0,
                promedioServicio: 0.0,
                minutos: 0.0
            });
            for (let index2 = 0; index2 < rangoSimulacion; index2++) {
                let auxLlegada = Math.round(poissonRandom(tLlegada));
                let auxServicio = 0;

                if (auxLlegada <= 0) {
                    auxServicio = 0;
                    auxLlegada = 0
                } else {
                    auxServicio = tServicio;
                }

                registro[index].clientes.push({
                    llegadacliente: auxLlegada,
                    servicioCliente: auxServicio,
                    minuto: auxMin
                });
                registro[index].promedioLlegada += auxLlegada;
                registro[index].promedioServicio += auxServicio;

                promedioLlegadaTotal += auxLlegada;
                promedioServicioTotal += auxServicio;

                auxMin++;

            }
            registro[index].promedioLlegada /= rangoSimulacion;
            registro[index].promedioServicio /= rangoSimulacion;
            registro[index].minutos = auxMin;

        }

        promedioLlegadaTotal /= minSimulacion;
        promedioServicioTotal /= minSimulacion;

        return (<div className="Simulacion-MD1">
            <table class="tg">
                <thead>
                    <tr>
                        <th class="tg-wt8g" colspan="6"> Simulación</th>
                    </tr>
                </thead>

                {registro.map((element) => {
                    return (
                        <tbody>
                            <tr>
                                <td class="tg-0pj9">Promedio llegada: </td>
                                <td class="tg-0pj9">{element.promedioLlegada.toFixed(4)} clientes/minuto.</td>
                                <td class="tg-0pj9">Promedio Servicio:</td>
                                <td class="tg-0pj9">{element.promedioServicio.toFixed(4)} clientes/minutos.</td>
                                <td class="tg-0pj9">Rango tiempo:</td>
                                <td class="tg-0pj9">{element.minutos - rangoSimulacion} - {element.minutos}</td>
                            </tr>
                            <tr>
                                <td class="tg-1wig" colspan="2">Clientes que llegaron:</td>
                                <td class="tg-1wig" colspan="2">Tiempo de servicio:</td>
                                <td class="tg-1wig" colspan="2">Minuto:</td>
                            </tr>
                            {
                                element.clientes.map((cliente) => {
                                    return (
                                        <tr>
                                            <td class="tg-baqh" colspan="2">{cliente.llegadacliente}</td>
                                            <td class="tg-baqh" colspan="2">{cliente.servicioCliente.toFixed(4)} minutos.</td>
                                            <td class="tg-baqh" colspan="2">{cliente.minuto}</td>
                                        </tr>)
                                })
                            }
                        </tbody>
                    )
                })}
            </table>
            <h1>Promedio llegada Total: {promedioLlegadaTotal.toFixed(4)} clientes/minuto. Promedio Servicio Total: {promedioServicioTotal.toFixed(4)} cliente/minuto.
                Timepo total de simulación: {auxMin} minutos.</h1>
        </div>)
    }


    return (
        <div className="Contorno-MD1">
            <a className="Titulo-MD1">Modelo M/D/1</a>
            <div className="Descripcion-MD1">
                <a>Distribución del tiempo de llegadas Markovianas (Poisson)</a>
                <a>Distribución del tiempo de servicio deterministica (Constante)</a>
                <a>Con un solo servidor</a>
            </div>
            <div className="Formulario-MD1">

                <label>
                    <a>λ: tasa promedio de llegada</a>
                    <input type="number" name="tasaLlegada" value={tLlegada}
                        onChange={(e) => {
                            setLlegada(parseFloat(e.target.value));
                        }} />
                    <a> clientes/minuto</a>
                </label>


                <label>
                    <a>μ: Tiempo de servicio servicio</a>
                    <input type="number" name="tasaServicio" value={tServicio}
                        onChange={(e) => {
                            setServicio(parseFloat(e.target.value));
                        }} />
                    <a> clientes/minuto</a>
                </label>

                <label>
                    <a>Minutos de simulación </a>
                    <input type="number" name="minutosSimulacion" value={minSimulacion}
                        onChange={(e) => {
                            setminSimulacion(parseFloat(e.target.value));
                        }} />
                    <a> con rango </a>
                    <input type="number" name="rengoSimulacion" value={rangoSimulacion}
                        onChange={(e) => {
                            setRangoSimulacion(parseFloat(e.target.value));
                        }} />
                </label>

                <div>
                    <button className="Boton-MD1" onClick={ordenar}>Ejecutar</button>
                    <button className="Boton-MD1" onClick={aleatorio}>Aleatorio</button>
                    <button className="Boton-MD1" onClick={limpiar}>Limpiar</button>
                </div>
            </div>
            {bEjecuta === true ? mostrarCaracterizacion()
                : <></>}
            {bEjecuta === true ? mostrarSimulacion() : <></>}
        </div>
    )
}

export default ModeloMD1;