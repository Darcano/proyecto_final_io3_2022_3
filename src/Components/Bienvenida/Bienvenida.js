import "./Bienvenida.css";
import expoDistribution from "../../Components/Distribuciones/exponencial";

function Bienvenido() {
  let arreglo = [];
  for (let index = 0; index < 40; index++) {
    arreglo.push(expoDistribution(3.7518));

  }
  return (
    <div className="Contorno">
      <a className="Titulo">Proyecto Final - Simuilación</a>
      <div className="Encapsulado">
        <a className="Nombre">Stivel Pinilla Puerta</a>
        <a className="Codigo">COD: 20191020024</a>
      </div>
      <div className="Encapsulado">
        <a className="Nombre">Yuneider Parada</a>
        <a className="Codigo">COD: 20191020058</a>
      </div>
      <div className="Encapsulado">
        <a className="Nombre">Universidad Distrital Francisco Jose de Caldas</a>
        <a className="Codigo">Facultad de Ingeniería</a>
        <a className="Codigo">Ingeniería de Sistemas</a>
        <a className="Codigo">Investigación de Operaciones III</a>
        <a className="Codigo">Bogota D.C.</a>
        <a className="Codigo">Diciembre 2022</a>
      </div>
    </div>
  )
}

export default Bienvenido;

