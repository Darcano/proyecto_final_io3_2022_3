import "./Side.css";
import { Sidebar, Menu, MenuItem, SubMenu } from 'react-pro-sidebar';
import { Link, Outlet } from "react-router-dom";

function SideBarCool() {
  return (
    <div className="container-all">
      <Sidebar>
        <Menu menuItemStyles={{
          button: ({ level, active, disabled }) => {
            // only apply styles on first level elements of the tree
            if (level === 0) {
              return {
                color: disabled ? '#0079cf' : '#0079cf',
                backgroundColor: active ? '#eecef9' : undefined,
              };
            }
            if (level === 1) {
              return {
                color: disabled ? '#ef1100' : '#ef1100',
                backgroundColor: active ? '#eecef9' : undefined,
              };
            }

          },
        }}>
          <MenuItem routerLink={<Link to="/"></Link>}> Portada </MenuItem>
          {/* <SubMenu label="Probabilidad">
            <MenuItem> Bernoulli </MenuItem>
            <MenuItem> Binomial </MenuItem>
            <MenuItem> Binomial Geometrica </MenuItem>
            <MenuItem> Binomial Negativa </MenuItem>
            <MenuItem> Hipergeometrica </MenuItem>
            <MenuItem> Poisson </MenuItem>
          </SubMenu> */}
          <MenuItem routerLink={<Link to="/TeoriaDesicion"></Link>}> Teoría de la Decisión </MenuItem>
          <MenuItem> Cadenas de Markov </MenuItem>
          <SubMenu label="Teoría de Colas">
            <MenuItem routerLink={<Link to="/TeoriaColas/MM1"></Link>}> M/M/1 </MenuItem>
            <MenuItem routerLink={<Link to="/TeoriaColas/MMS"></Link>}> M/M/S </MenuItem>
            <MenuItem routerLink={<Link to="/TeoriaColas/MG1"></Link>}> M/G/1 </MenuItem>
            <MenuItem routerLink={<Link to="/TeoriaColas/MD1"></Link>}> M/D/1 </MenuItem>
          </SubMenu>
        </Menu>
      </Sidebar>
      <Outlet />
    </div>
  )
}

export default SideBarCool;

