function factorialJS(n) {
    var total = 1;
    for (let i = 1; i <= n; i++) {
        total = total * i;
    }
    return total;
}

export default factorialJS;