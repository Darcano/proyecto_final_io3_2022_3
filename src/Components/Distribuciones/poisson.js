function poissonRandom(media = 100) {
    var L = Math.exp(-media);
    var p = 1.0;
    var k = 0;

    do {
        k++;
        p *= Math.random();
    } while (p > L);

    return k - 1;
}

export default poissonRandom;