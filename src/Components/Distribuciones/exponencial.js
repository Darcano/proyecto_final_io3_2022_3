function expoDistribution(lambda) {
    return -Math.log(1.0 - Math.random()) * lambda;
}

export default expoDistribution;