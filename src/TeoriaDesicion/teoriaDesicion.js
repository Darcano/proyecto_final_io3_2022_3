import "./teoriaDesicion.css"

import { useState } from 'react';

import gaussianRandom from "../Components/Distribuciones/normal";

function TeoriaDesicion() {
    const [pV, setPV] = useState(4500);
    const [costo, setCosto] = useState(3100);
    const [pR, setPR] = useState(2500);
    const [pBN, setPBN] = useState(300);
    const [frecuencia, setFrecuencia] = useState([{
        valor: 30,
        valorFrec: 5
    }, {
        valor: 31,
        valorFrec: 9
    }, {
        valor: 32,
        valorFrec: 26
    }, {
        valor: 33,
        valorFrec: 38
    }, {
        valor: 34,
        valorFrec: 18
    }, {
        valor: 35,
        valorFrec: 4
    }]);

    const [gM, setGM] = useState(0);
    const [pM, setPM] = useState(0);
    const [uM, setUM] = useState(0);

    const [optimistaV, setOptimistaV] = useState({
        valor: 0,
        alternativa: 0
    });
    const [pesimistaV, setPesimistaV] = useState({
        valor: 0,
        alternativa: 0
    });

    const [modeloSelect, setModeloSelect] = useState(1);
    const [diaSimulacion, setDiaSimulacion] = useState(20);

    const [refrescar, setRefrescar] = useState(false);
    const [estadoEje, setEstadoEje] = useState(false);

    function aleatorio() {
        let auxPV = 0;
        let auxcosto = 0;
        let auxPR = 0;
        let auxPBN = 0;
        do {
            auxPV = gaussianRandom(5000, 600);
            auxcosto = gaussianRandom(3500, 400);
            auxPR = gaussianRandom(2000, 200);
            auxPBN = gaussianRandom(300, 60);
        } while (auxPV > auxcosto && auxcosto > auxPR);
        setPV(parseInt(auxPV));
        setCosto(parseInt(auxcosto));
        setPR(parseInt(auxPR));
        setPBN(parseInt(auxPBN));
    }

    function limpiar() {
        setPV(4500);
        setCosto(3100);
        setPR(2500);
        setPBN(300);
        setFrecuencia(([{
            valor: 30,
            valorFrec: 5
        }, {
            valor: 31,
            valorFrec: 9
        }, {
            valor: 32,
            valorFrec: 26
        }, {
            valor: 33,
            valorFrec: 38
        }, {
            valor: 34,
            valorFrec: 18
        }, {
            valor: 35,
            valorFrec: 4
        }]));
        setEstadoEje(false);
    }


    function agregarFila() {
        let auxFrecuencia = frecuencia;
        auxFrecuencia.push({
            valor: 1,
            valorFrec: 1
        })
        setFrecuencia(auxFrecuencia);
        setRefrescar(!refrescar);
    }

    function eliminarFila() {
        let auxFrecuencia = frecuencia;
        auxFrecuencia.pop();
        setFrecuencia(auxFrecuencia);
        setRefrescar(!refrescar);
    }

    function ordenar() {
        let auxGM = pV - costo;
        let auxPM = costo - pR;
        let axuUM = (pM) / (gM + pM);

        setGM(auxGM);
        setPM(auxPM);
        setUM(axuUM);
        setEstadoEje(true);
    }

    function mostrarParametros() {
        let auxMatriz = [];
        for (let index = 0; index < frecuencia.length; index++) {
            auxMatriz.push([]);

        }

        let mediaSimulacion = 0;
        let varianzaSimulacion = 0;
        let auxCantProb = 0;

        for (let index = 0; index < frecuencia.length; index++) {
            mediaSimulacion += frecuencia[index].valor * frecuencia[index].valorFrec;
            auxCantProb += frecuencia[index].valorFrec;
        }
        mediaSimulacion = mediaSimulacion / auxCantProb;
        for (let index = 0; index < frecuencia.length; index++) {
            varianzaSimulacion += (Math.pow(frecuencia[index].valor - mediaSimulacion, 2)) * frecuencia[index].valorFrec;
        }
        varianzaSimulacion = varianzaSimulacion / auxCantProb;

        return (<div className="contenedor-matriz-desicion">
            <table className="tg-matriz-desicion">
                <thead>
                    <tr >
                        <th className="tg-matriz-desicion-1wig" colspan={`${frecuencia.length + 1}`}>MATRIZ</th>
                    </tr>
                    <tr>
                        <th className="tg-matriz-desicion-1wig">ALTERNATIVAS</th>
                        {frecuencia.map((element) => {
                            return (<th className="tg-matriz-desicion-1wig">{element.valor}</th>)
                        })}
                    </tr>
                </thead>
                <tbody>
                    {frecuencia.map((element, indexOri) => {
                        return (<tr>
                            <td className="tg-matriz-desicion-1wig">{element.valor}</td>
                            {frecuencia.map((element2, index) => {
                                let auxSelect = 0;
                                let auxCalculo = 0;
                                if (element.valor == element2.valor) {
                                    auxSelect = 1
                                    auxCalculo = element.valor * gM;
                                    auxMatriz[indexOri].push(auxCalculo);
                                    return (
                                        <td className="tg-matriz-desicion-0lax">{auxCalculo}</td>
                                    )
                                }
                                if (element.valor > element2.valor) {
                                    auxSelect = 2
                                    auxCalculo = (element2.valor * pV) - (element.valor * costo) + ((element.valor - element2.valor) * pR);
                                    auxMatriz[indexOri].push(auxCalculo);
                                    return (
                                        <td className="tg-matriz-desicion-0lax">{auxCalculo}</td>
                                    )
                                }
                                if (element.valor < element2.valor) {
                                    auxSelect = 3
                                    auxCalculo = (element.valor * pV) - (element.valor * costo) - ((element2.valor - element.valor) * pBN);
                                    auxMatriz[indexOri].push(auxCalculo);
                                    return (
                                        <td className="tg-matriz-desicion-0lax">{auxCalculo}</td>
                                    )
                                }
                                if (auxSelect == 0) {
                                    auxCalculo = "NUL";
                                    auxMatriz[indexOri].push(auxCalculo);
                                    return (
                                        <td className="tg-matriz-desicion-0lax">{auxCalculo}</td>
                                    )
                                }

                            })}
                        </tr>)
                    })}
                </tbody>
            </table>
            <h2>Metodos</h2>
            <table className="tg-optimista">
                <thead>
                    <tr>
                        <th className="tg-optimista-j6lv">OPTIMISTA</th>
                    </tr>
                </thead>

                <tbody>
                    {frecuencia.map((element, index) => {
                        let auxMayor = 0;
                        console.log("MATRIZ", JSON.parse(JSON.stringify(auxMatriz)));
                        for (let index2 = 0; index2 < frecuencia.length; index2++) {
                            console.log("ValorPOS:", auxMatriz[index][index2], ":", index, "-", index2);
                            if (auxMayor <= auxMatriz[index][index2]) {
                                auxMayor = auxMatriz[index][index2];
                            }
                        }

                        if (auxMayor > optimistaV.valor) {
                            setOptimistaV({
                                valor: auxMayor,
                                alternativa: index
                            });
                        }

                        return (<tr>
                            <td className={auxMayor == optimistaV.valor ? `${"tg-optimista-kjfa"}` : "tg-optimista-amwm"}>{auxMayor}</td>
                        </tr>)
                    })}
                </tbody>
            </table>
            <table className="tg-optimista">
                <thead>
                    <tr>
                        <th className="tg-optimista-j6lv">PESIMISTA</th>
                    </tr>
                </thead>

                <tbody>
                    {frecuencia.map((element, index) => {
                        let auxMayor = 100000;
                        console.log("MATRIZ", JSON.parse(JSON.stringify(auxMatriz)));
                        for (let index2 = 0; index2 < frecuencia.length; index2++) {
                            console.log("ValorPOS:", auxMatriz[index][index2], ":", index, "-", index2);
                            if (auxMayor >= auxMatriz[index][index2]) {
                                auxMayor = auxMatriz[index][index2];
                            }
                        }

                        if (auxMayor > pesimistaV.valor) {
                            setPesimistaV({
                                valor: auxMayor,
                                alternativa: index
                            });
                        }

                        return (<tr>
                            <td className={auxMayor == pesimistaV.valor ? `${"tg-optimista-kjfa"}` : "tg-optimista-amwm"}>{auxMayor}</td>
                        </tr>)
                    })}
                </tbody>
            </table>
            <h2>Simulación por modelo</h2>
            <label>
                <select value={modeloSelect} onChange={(e) => {
                    setModeloSelect(parseInt(e.target.value));
                }}>
                    <option value="1">Optimista</option>
                    <option value="2">Pesimista</option>
                </select>
            </label>
            {mostrarSimulacion(mediaSimulacion, varianzaSimulacion)}

        </div >)

    }

    function mostrarSimulacion(media, varianza) {
        let auxRandom = [];
        let auxSeleccion = 0;
        let auxGanaciaTotal = 0;
        console.log("Media:", media, "Varianza:", varianza, "Desviacion:", Math.sqrt(varianza));
        for (let index = 0; index < diaSimulacion; index++) {
            auxRandom.push(parseInt(gaussianRandom(media, Math.sqrt(varianza))));
        }
        if (modeloSelect == 1) {
            auxSeleccion = frecuencia[optimistaV.alternativa].valor;
        }
        if (modeloSelect == 2) {
            auxSeleccion = frecuencia[pesimistaV.alternativa].valor;
        }

        return (<table class="tg-simulacion-desicion">
            <thead>
                <tr>
                    <th class="tg-simulacion-desicion-jwtr" colspan="4">Simulación</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="tg-simulacion-desicion-amwm">Día</td>
                    <td class="tg-simulacion-desicion-amwm">Pedidos</td>
                    <td class="tg-simulacion-desicion-amwm">Elección</td>
                    <td class="tg-simulacion-desicion-amwm">Ganancia</td>
                </tr>
                {auxRandom.map((element, index) => {
                    let gananciaDia = 0;
                    if (element > auxSeleccion) {
                        gananciaDia = (auxSeleccion * pV) - (auxSeleccion * costo) - ((element - auxSeleccion) * pBN);
                    }
                    if (element < auxSeleccion) {
                        gananciaDia = (element * pV) - (auxSeleccion * costo) + ((auxSeleccion - element) * pR);
                    }
                    if (element == auxSeleccion) {
                        gananciaDia = element * gM;
                    }
                    auxGanaciaTotal += gananciaDia;
                    return (<tr>
                        <td class="tg-simulacion-desicion-baqh">{index + 1}</td>
                        <td class="tg-simulacion-desicion-baqh">{element}</td>
                        <td class="tg-simulacion-desicion-baqh">{auxSeleccion}</td>
                        <td class="tg-simulacion-desicion-baqh">{gananciaDia}</td>
                    </tr>)
                })}
                <tr>
                    <td class="tg-simulacion-desicion-amwm" colspan="3">Ganacia Total</td>
                    <td class="tg-simulacion-desicion-amwm">{auxGanaciaTotal}</td>
                </tr>

            </tbody>
        </table>)
    }

    return (<div className="Contorno-Desicion">
        <a className="Titulo-Desicion">Teoría de la decisión</a>
        <div className="Formulario-Desicion">

            <label>
                <a>P.V. : Valor de venta</a>
                <input type="number" name="valorVenta" value={pV}
                    onChange={(e) => {
                        setPV(parseFloat(e.target.value));
                    }} />
            </label>


            <label>
                <a>Costo de producción</a>
                <input type="number" name="costoProduccion" value={costo}
                    onChange={(e) => {
                        setCosto(parseFloat(e.target.value));
                    }} />
            </label>

            <label>
                <a>P.R. : Precio de recuperación</a>
                <input type="number" name="precioRecuperacion" value={pR}
                    onChange={(e) => {
                        setPR(parseFloat(e.target.value));
                    }} />
            </label>

            <label>
                <a>P.B.N. : Perdida del Buen Nombre</a>
                <input type="number" name="precioRecuperacion" value={pBN}
                    onChange={(e) => {
                        setPBN(parseFloat(e.target.value));
                    }} />
            </label>

            <div className="Frecuencia-Desicion">
                <table className="tg">
                    <thead>
                        <tr>
                            <th className="tg-6hok">Cant.</th>
                            <th className="tg-6hok">Frecuencia</th>
                        </tr>
                    </thead>

                    <tbody>
                        {frecuencia.map((element, index) => {
                            return (<tr>
                                <td className="tg-amwm"><input className="inputTabla-Desicion" type="number" name="precioRecuperacion" value={element.valor}
                                    onChange={(e) => {
                                        let auxFrecuencia = frecuencia;
                                        let auxValor = parseInt(e.target.value);
                                        auxFrecuencia[index].valor = auxValor;
                                        setFrecuencia(auxFrecuencia);
                                        setRefrescar(!refrescar);
                                    }} /></td>
                                <td className="tg-amwm"><input className="inputTabla-Desicion" type="number" name="precioRecuperacion" value={element.valorFrec}
                                    onChange={(e) => {
                                        let auxFrecuencia = frecuencia;
                                        let auxValorFrecu = parseInt(e.target.value);
                                        auxFrecuencia[index].valorFrec = auxValorFrecu;
                                        setFrecuencia(auxFrecuencia);
                                        setRefrescar(!refrescar);
                                    }} /></td>
                            </tr>)
                        })}
                    </tbody>
                </table>
                <button className="Boton-Desicion" onClick={agregarFila}>Agregar</button>
                <button className="Boton-Desicion" onClick={eliminarFila}>Eliminar</button>
            </div>

            <label>
                <a>Días de la simulación </a>
                <input type="number" name="diasSimulacion" value={diaSimulacion}
                    onChange={(e) => {
                        if (parseInt(e.target.value) > 0) { setDiaSimulacion(parseInt(e.target.value)); }
                    }} />
            </label>

            <div>
                <button className="Boton-Desicion" onClick={ordenar}>Ejecutar</button>
                <button className="Boton-Desicion" onClick={aleatorio}>Aleatorio</button>
                <button className="Boton-Desicion" onClick={limpiar}>Limpiar</button>
            </div>
        </div>
        {estadoEje ? mostrarParametros() : <></>}
    </div>)

}

export default TeoriaDesicion;
